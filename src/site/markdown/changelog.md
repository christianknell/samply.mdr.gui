# Changelog

## Version 1.9.1 (unreleased)

- Add optional translation for definitions
- Remove omnifaces and weld dependencies

## Version 1.9.0

- added support for the caDSR
- added support for the export and import of dataelements
- switched to the open source repository

## Version 1.8.1

- fix a bug in the search view

## Version 1.8.0

- **Namespace for the Postgres configuration file changed**
- Renamed the configuration file to `mdr.oauth2.xml`
- The "My Drafts" view now offers a new button which releases all marked
  elements
- the view site now works with the back button from the browser
- the catalog wizard now verifies the uploaded catalog using the XSD
  file from the mdr-catalog-xsd project
- the new OAuth2 web filter handles the Samply Auth login with code,
  the old samplyLogin.xhtml is just a redirect from the Maintenance (the first
  web filter) to index.xhtml
- the old "My Namespaces" view has been replaced. The "view" shows the
  owned namespaces separately.
- added a textfield with the URN in the detail view
- added the maven site documentation
- added the export and import functionality

## Version 1.7.0

- Added more than one hostname to the oauth2 configuration file
- the search method now checks if the text is a valid urn, if it is, it
  shows the element as result
- the permissible values are now shown as a table in readonly mode
- the element wizard now adds the necessary slots and definitions if required
- the namespaces in the View view are now ordered
- the project name can be parametrized using the context parameter 'projectName'
- the ranges of value domains are now shows as '234<=x<=1232' if applicable
- fixed a bug that prevented the creation of namespaces for OSSE users

## Version 1.6.0

- Added namespace constraints (required languages and slots for new elements)
- Added a new option for groups that allows updating a group and all its
  elements, provides also an overview of the update process. Also shows a
  dialog that informs the user about the process.
- Implemented the "defaultGrant" configuration
- The stylesheets and javascript files are now loaded by script and link tags,
  with an additional version parameter to prevent caching across different
  versions
- the navigation bar at the bottom of the page used in the wizard has no
  longer the same background color as the top navigation bar. The wizard
  itself has a slightly gray background color, so that the user can identify
  the complete wizard
- added support for catalogs
