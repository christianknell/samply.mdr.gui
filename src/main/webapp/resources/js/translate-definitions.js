$(document).ready(function() {
    $(".content").on("click", ".auto-translate-button", function() {
        var el = $(this).parents('.well').find('select.language_selection');
        translate(el);
    });
});

/*
 * Function for receiving the Title of the first DefinitionElement
 */
function getElementSourceTitle() {
    var definitionContainer = $(".definition_container").first();
    var elementSourceTitle = definitionContainer.find(".elementTitle").first().val();
    return elementSourceTitle;
}
/*
 * Function for receiving the Language of the first DefinitionElement
 */
function getElementSourceLanguage() {
    var definitionContainer = $(".definition_container").first();
    var elementSourceLanguage = definitionContainer.find(".language_selection:eq(1)").val();
    return elementSourceLanguage;
}

/*
 * Function that translates the title of the first DefinitionElement into the desired language
 * @param elementTitle: the title that should be translated
 * @param sourceLanguage: the language of the original title
 * @param destinationLanguage: the language the original title should be translated to
 * @param eventElement: the newly added DefinitionElement
 */
function translateElement(elementTitle, sourceLanguage, destinationLanguage, eventElement) {
    var translatedElementTitle = elementTitle;
    var lang = sourceLanguage.toLowerCase() + "-" + destinationLanguage.toLowerCase();
    // Making an AJAX-Call to the translation service
    $.ajax({
        url: "https://translate.yandex.net/api/v1.5/tr.json/translate",
        type: "POST",
        data: {
            "key": "trnsl.1.1.20170803T084457Z.bbb8d4acb47b3a7f.d637a48aa2e9f7e79e3219568669f03d8850e446",
            "text": elementTitle.toString(),
            "lang": lang.toString()
        },
        success: function(data, status, xhr) {
            translatedElementTitle = data.text[0];
            replaceElementTitle(eventElement, translatedElementTitle);
        },
    });
    return translatedElementTitle;
}
/*
 * Function for replacing the title of a DefinitionElement
 * @param eventElement: the newly added DefinitionElement
 * @param newElementTitle: the new title of the DefinitionElement
 */
function replaceElementTitle(eventElement, newElementTitle) {
    // GET PARENT NODE WITH CLASS "DEFINITION_CONTAINER"
    var definitionContainer = eventElement.parents(".definition_container");
    if(definitionContainer.length > 0) {
        var elementDestinationTitleField = $(definitionContainer).find(".elementTitle").first();
        elementDestinationTitleField.val(newElementTitle);
    } else {
        alert("error");
    }
}
/*
 * Function for starting the translation and replacing the translated title
 * @param eventElement: the newly added DefinitionElement
 */
function translate(eventElement) {
    var elementSourceTitle = getElementSourceTitle();
    if (elementSourceTitle.length > 0) {
        var elementSourceLanguage = getElementSourceLanguage();
        translateElement(elementSourceTitle, elementSourceLanguage, eventElement.val(), eventElement);
    } else {
        console.log("empty source designation. not calling translation service");
    }
}