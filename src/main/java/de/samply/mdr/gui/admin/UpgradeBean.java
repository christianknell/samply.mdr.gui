/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui.admin;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.gui.ConnectionFactory;
import de.samply.mdr.gui.MDRConfig;
import de.samply.mdr.util.Message;
import de.samply.sdao.SQLUpgradeException;
import de.samply.sdao.Upgrader;

/**
 * This bean manages the upgrade process in the admin interface.
 */
@ManagedBean
@ViewScoped
public class UpgradeBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * If true, the dry run has been successful.
     */
    private boolean dryRunSuccess = false;

    /**
     * The upgrader that executes the upgrade
     */
    private transient Upgrader upgrader;

    /**
     * Starts the database upgrade.
     * @param dry
     * @return
     */
    public String run(boolean dry) {
        try(MDRConnection auth = ConnectionFactory.get()) {
            upgrader = new Upgrader(auth);

            upgrader.upgrade(dry);

            dryRunSuccess = true;

            /**
             * If the user wants a real upgrade run, commit the transaction,
             * reload the config and redirect him back to the index web page.
             */
            if(dry == false) {
                auth.commit();
                MDRConfig.reloadConfig(auth);
                return "/admin/index?faces-redirect=true";
            }

            Message.addInfo("dryRunSuccessful");
        } catch (SQLUpgradeException e) {
            Message.addError("contactServerAdmin");
            Message.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage(), false);
            System.out.println("SQL Upgrade Exception!");
        } catch (Exception e) {
            e.printStackTrace();
            Message.addError("contactServerAdmin");
        }
        return null;
    }

    /**
     * @return the dryRunSuccess
     */
    public boolean isDryRunSuccess() {
        return dryRunSuccess;
    }

    /**
     * @param dryRunSuccess the dryRunSuccess to set
     */
    public void setDryRunSuccess(boolean dryRunSuccess) {
        this.dryRunSuccess = dryRunSuccess;
    }

}
