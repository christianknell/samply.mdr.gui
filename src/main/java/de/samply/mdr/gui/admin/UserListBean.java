/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui.admin;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import de.samply.mdr.dal.dto.User;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.UserDAO;
import de.samply.mdr.gui.ConnectionFactory;
import de.samply.sdao.DAOException;

/**
 * The bean manages the user list in the admin interface
 * @author paul
 *
 */
@ManagedBean
@ViewScoped
public class UserListBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The list of users.
     */
    private List<User> users;

    @PostConstruct
    public void initialize() {
        try(MDRConnection mdr = ConnectionFactory.get()) {
            users = mdr.get(UserDAO.class).getUsers();
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Toggles the given users permission to create namespaces.
     * @param user
     * @return
     */
    public String toggle(User user) {
        try(MDRConnection mdr = ConnectionFactory.get()) {
            if(user.getCanCreateNamespace()) {
                user.setCanCreateNamespace(false);
            } else {
                user.setCanCreateNamespace(true);
            }

            mdr.get(UserDAO.class).updatePermissions(user);
            mdr.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Toggles the given users permission to upload catalogs.
     * @param user
     * @return
     */
    public String toggleCatalog(User user) {
        try(MDRConnection mdr = ConnectionFactory.get()) {
            if(user.getCanCreateCatalog()) {
                user.setCanCreateCatalog(false);
            } else {
                user.setCanCreateCatalog(true);
            }

            mdr.get(UserDAO.class).updatePermissions(user);
            mdr.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Toggles the given users permission to export/import elements
     * @param user
     * @return
     */
    public String toggleExport(User user) {
        try(MDRConnection mdr = ConnectionFactory.get()) {
            if(user.getCanExportImport()) {
                user.setCanExportImport(false);
            } else {
                user.setCanExportImport(true);
            }

            mdr.get(UserDAO.class).updatePermissions(user);
            mdr.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * @return the users
     */
    public List<User> getUsers() {
        return users;
    }

    /**
     * @param users the users to set
     */
    public void setUsers(List<User> users) {
        this.users = users;
    }

}
