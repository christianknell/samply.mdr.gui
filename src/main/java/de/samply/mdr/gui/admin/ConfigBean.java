/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui.admin;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.Vocabulary;
import de.samply.mdr.gui.ConnectionFactory;
import de.samply.mdr.gui.MDRConfig;
import de.samply.sdao.DAOException;
import de.samply.sdao.json.JSONResource;

/**
 * This bean manages the configuration in the admin interface.
 */
@ViewScoped
@ManagedBean
public class ConfigBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * If true, new users can create namespaces without explicit permission.
     */
    private Boolean namespaceGrant;

    /**
     * If true, new users can create catalogs without explicit permission.
     */
    private Boolean catalogGrant;

    /**
     * If true, new users can export and import elements without explicit permission.
     */
    private Boolean exportImportGrant;

    /**
     * If true, show the button on definitions to use an external translation service
     */
    private Boolean showTranslationButton;

    /**
     * <p>init.</p>
     */
    @PostConstruct
    public void init() {
        namespaceGrant = MDRConfig.getNamespaceGrant();
        catalogGrant = MDRConfig.getCatalogsGrant();
        exportImportGrant = MDRConfig.getExportImportGrant();
        showTranslationButton = MDRConfig.isShowTranslateDefinitionButton();
    }


    public String saveConfig() {
        try(MDRConnection mdr = ConnectionFactory.get()) {
            JSONResource config = mdr.getConfig();
            config.setProperty(Vocabulary.DEFAULT_NAMESPACE_GRANT, namespaceGrant);
            config.setProperty(Vocabulary.DEFAULT_CATALOG_GRANT, catalogGrant);
            config.setProperty(Vocabulary.DEFAULT_EXPORT_IMPORT_GRANT, exportImportGrant);
            config.setProperty(Vocabulary.SHOW_TRANSLATION_BUTTON, showTranslationButton);
            mdr.saveConfig(config);
            mdr.commit();

            MDRConfig.reloadConfig(mdr);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @return the namespaceGrant
     */
    public Boolean getNamespaceGrant() {
        return namespaceGrant;
    }

    /**
     * @param namespaceGrant the namespaceGrant to set
     */
    public void setNamespaceGrant(Boolean namespaceGrant) {
        this.namespaceGrant = namespaceGrant;
    }

    /**
     * @return the catalogGrant
     */
    public Boolean getCatalogGrant() {
        return catalogGrant;
    }

    /**
     * @param catalogGrant the catalogGrant to set
     */
    public void setCatalogGrant(Boolean catalogGrant) {
        this.catalogGrant = catalogGrant;
    }


    /**
     * @return the exportImportGrant
     */
    public Boolean getExportImportGrant() {
        return exportImportGrant;
    }


    /**
     * @param exportImportGrant the exportImportGrant to set
     */
    public void setExportImportGrant(Boolean exportImportGrant) {
        this.exportImportGrant = exportImportGrant;
    }

    /**
     * @return true if the translation button for definitions should be show, false otherwise
     */
    public Boolean getShowTranslationButton() {
        return showTranslationButton;
    }


    /**
     * @param showTranslationButton if true, the button to translate definitions will be shown
     */
    public void setShowTranslationButton(Boolean showTranslationButton) {
        this.showTranslationButton = showTranslationButton;
    }
}
