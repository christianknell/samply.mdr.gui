/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui.beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.ElementType;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dao.DefinitionDAO;
import de.samply.mdr.dao.ElementDAO;
import de.samply.mdr.dao.IdentifiedDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.ScopedIdentifierDAO;
import de.samply.mdr.dao.utils.Importer;
import de.samply.mdr.gui.BeanValidators;
import de.samply.mdr.gui.ConnectionFactory;
import de.samply.mdr.gui.MDRElement;
import de.samply.mdr.gui.ValueDomainDTO;
import de.samply.mdr.gui.WizardStep;
import de.samply.mdr.gui.exceptions.ValidationException;
import de.samply.mdr.lib.Constants.DatatypeField;
import de.samply.mdr.lib.AdapterDefinition;
import de.samply.mdr.lib.AdapterPermissibleValue;
import de.samply.mdr.util.JSFUtil;
import de.samply.mdr.util.Mapper;
import de.samply.mdr.util.Message;
import de.samply.sdao.DAOException;

/**
 * This wizard allows *basic* changes only, that means:
 * <pre>
 * * definitions and designations (also the labels for permitted values)
 * * members (for groups)
 * * members that identify the same identified item (for records)
 * * slots
 * </pre>
 * @author paul
 *
 */
@SessionScoped
@ManagedBean
public class ChangeWizard extends DataElementGroupWizard {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private MDRElement element;

    private List<WizardStep> steps;

    private ValueDomainDTO valueDomain;

    @Override
    public String validateSlots() {
        return BeanValidators.validateSlots(getSlots()) ? "changeverify?faces-redirect=true" : null;
    }

    @Override
    public String validateDefinitions() {
        setStarted(true);
        if(BeanValidators.validateDefinitions(getDefinitions())) {
            switch(element.getElement().getElementType()) {
            case DATAELEMENT:
                if(valueDomain.getDatatype() == DatatypeField.LIST) {
                    return "changevalidation?faces-redirect=true";
                } else {
                    return "changeslots?faces-redirect=true";
                }

            case DATAELEMENTGROUP:
            case RECORD:
            default:
                return "changeassignment?faces-redirect=true";
            }
        } else {
            return null;
        }
    }

    @Override
    public ElementType getElementType() {
        if(element != null) {
            return element.getElement().getElementType();
        } else {
            return ElementType.DATAELEMENT;
        }
    }

    @Override
    public String finish() {
        String vDefinitions = validateDefinitions();
        String vIdentification = validateIdentification();
        String vMembers = validateMembers();

        if(vDefinitions == null || vIdentification == null || vMembers == null) {
            return null;
        }

        try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
            Namespace namespace = mdr.get(ElementDAO.class).getNamespace(getNamespace());

            ScopedIdentifier identifier = super.finish(element.getElement().getId(),
                    namespace.getId(), mdr);

            if(element.getElement().getElementType() == ElementType.DATAELEMENT) {
                if(valueDomain.getDatatype() == DatatypeField.LIST) {
                    List<AdapterPermissibleValue> values = valueDomain.getPermittedValues();
                    for(AdapterPermissibleValue value : values) {
                        for(AdapterDefinition definition : value.getDefinitions()) {
                            Definition def = Mapper.convert(definition);
                            def.setElementId(value.getId());
                            def.setScopedIdentifierId(identifier.getId());
                            mdr.get(DefinitionDAO.class).saveDefinition(def);
                        }
                    }
                }
            } else {
                Importer importer = new Importer();

                int index = 1;
                for(MDRElement element : getElements()) {
                    ScopedIdentifier sub = importer.importElement(mdr, element.getUrn(), identifier.getNamespace(), getUserBean().getUserId());
                    mdr.get(ScopedIdentifierDAO.class).addSubIdentifier(identifier.getId(), sub.getId(), index++);
                }
            }

            mdr.commit();

            return done(mdr);
        } catch (DAOException e) {
            e.printStackTrace();
            Message.addError("serverError");
            return null;
        } catch (ValidationException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Initializes this wizard for the given URN.
     * @param urn
     * @return
     */
    public String initialize(String urn) {
        try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
            element = template(urn, mdr);

            steps = new ArrayList<>();
            steps.add(new WizardStep(JSFUtil.getString("definitions"), 1, "/user/changedefinitions.xhtml"));

            if(element.getElement().getElementType() == ElementType.DATAELEMENT) {
                valueDomain = Mapper.createValueDomain(element.getElement(), mdr, getUserBean().getSelectedLanguage());

                if(valueDomain.getDatatype() == DatatypeField.LIST) {
                    steps.add(new WizardStep(JSFUtil.getString("validation"), 2, "/user/changevalidation.xhtml"));
                }
            } else if(element.getElement().getElementType() == ElementType.DATAELEMENTGROUP) {
                setElements(Mapper.convertElements(mdr.get(IdentifiedDAO.class).getSubMembers(urn),
                        getUserBean().getSelectedLanguage(), true));
                steps.add(new WizardStep(JSFUtil.getString("membersShort"), 2, "/user/changeassignment.xhtml"));
            } else {
                setElements(Mapper.convertElements(mdr.get(IdentifiedDAO.class).getEntries(urn),
                        getUserBean().getSelectedLanguage(), false));
                steps.add(new WizardStep(JSFUtil.getString("entries"), 2, "/user/changeassignment.xhtml"));
            }

            steps.add(new WizardStep(JSFUtil.getString("slots"), 3, "/user/changeslots.xhtml"));
            steps.add(new WizardStep(JSFUtil.getString("verification"), 4, "/user/changeverify.xhtml"));

        } catch (DAOException e) {
            reset();
            return null;
        }
        return "/user/changedefinitions.xhtml?faces-redirect=true";
    }

    public String validateValidation() {
        return BeanValidators.validateValidation(valueDomain) ? "changeslots?faces-redirect=true" : null;
    }

    /* (non-Javadoc)
     * @see de.samply.mdr.gui.beans.DataElementGroupWizard#elementValid(de.samply.mdr.gui.MDRElement)
     */
    @Override
    protected boolean elementValid(MDRElement foundElement) {
        /**
         * Records allows dataelements only.
         */
        if(getElementType() == ElementType.RECORD) {
            return foundElement.getElement().getElementType() == ElementType.DATAELEMENT;
        } else {
            return super.elementValid(foundElement);
        }
    }

    /**
     * If the element we are changing is a record, check its dataelements for identity
     * @return
     */
    @Override
    public String validateMembers() {
        if(getElementType() == ElementType.RECORD) {
            try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
                List<IdentifiedElement> entries = mdr.get(IdentifiedDAO.class).getEntries(element.getUrn());

                if(getElements().size() != entries.size()) {
                    Message.addError("invalidMemberCount");
                    return null;
                }

                for(int i = 0; i < entries.size(); ++i) {
                    IdentifiedElement element = mdr.get(IdentifiedDAO.class).getElement(getElements().get(i).getUrn());
                    if(element.getId() != entries.get(i).getId()) {
                        Message.addError("invalidElement", element.getScoped().toString());
                        return null;
                    }
                }
                return "changeslots?faces-redirect=true";
            } catch (DAOException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return "changeslots?faces-redirect=true";
        }
    }

    @Override
    public String reset() {
        if(element != null) {
            return "/detail?faces-redirect=true&urn=" + element.getUrn();
        } else {
            return null;
        }
    }

    @Override
    public List<WizardStep> getSteps() {
        return steps;
    }

    @Override
    public Boolean getChange() {
        return true;
    }

    public ValueDomainDTO getValueDomain() {
        return valueDomain;
    }

    public void setValueDomain(ValueDomainDTO valueDomain) {
        this.valueDomain = valueDomain;
    }

}
