/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui.beans;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import de.samply.mdr.dal.dto.DataElementGroup;
import de.samply.mdr.dal.dto.ElementType;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dao.ElementDAO;
import de.samply.mdr.dao.IdentifiedDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.ScopedIdentifierDAO;
import de.samply.mdr.dao.utils.Importer;
import de.samply.mdr.gui.BeanValidators;
import de.samply.mdr.gui.ConnectionFactory;
import de.samply.mdr.gui.MDRElement;
import de.samply.mdr.gui.WizardStep;
import de.samply.mdr.gui.exceptions.DuplicateIdentifierException;
import de.samply.mdr.gui.exceptions.ValidationException;
import de.samply.mdr.util.JSFUtil;
import de.samply.mdr.util.Mapper;
import de.samply.mdr.util.Message;
import de.samply.sdao.DAOException;

/**
 * The wizard used to create a new data element group (or use an existing
 * data element group as a template) or change a data element group
 * in the first draft status.
 * @author paul
 *
 */
@ManagedBean
@SessionScoped
public class DataElementGroupWizard extends ElementWizard {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The list of elements that are currently in the group.
     */
    private List<MDRElement> elements = new ArrayList<>();

    /**
     * The list of URNs that have been dropped in the left panel.
     */
    private String items = "";

    public DataElementGroupWizard() {
    }

    @Override
    public String reset() {
        elements.clear();
        items = "";

        setStarted(false);

        return super.reset();
    }

    /**
     * Refreshes the array of elements.
     * Called when the user dropped a new element on the left panel.
     */
    public void refreshItems() {
        String[] items = this.items.split(",");
        List<MDRElement> old = elements;
        elements = new ArrayList<>();

        for(String item : items) {
            if(item.trim().length() == 0) {
                continue;
            }

            MDRElement foundElement = findElement(item, old);
            if(elementValid(foundElement)) {
                elements.add(foundElement);
            }
        }
    }

    /**
     * Checks, if the element is a valid member of this group.
     * @param foundElement
     * @return
     */
    protected boolean elementValid(MDRElement foundElement) {
        /**
         * Groups can contain everything.
         */
        return true;
    }

    @Override
    public String template(String urn) {
        try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
            template(urn, mdr);
            elements = Mapper.convertElements(mdr.get(IdentifiedDAO.class).getSubMembers(urn),
                    getUserBean().getSelectedLanguage(), true);
        } catch (DAOException e) {
            reset();
            return null;
        }

        return "/user/groupdefinitions?faces-redirect=true";
    }

    /**
     * Finds the
     * @param s
     * @param old
     * @return
     */
    private MDRElement findElement(String item, List<MDRElement> old) {
        for(MDRElement element : old) {
            if(element.getUrn().equals(item)) {
                return element;
            }
        }

        for(MDRElement element : getUserBean().getStarred()) {
            if(element.getUrn().equals(item)) {
                return element;
            }
        }

        try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
            return Mapper.convert(mdr.get(IdentifiedDAO.class).getElement(item), getUserBean().getSelectedLanguage());
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Validates the slots.
     */
    @Override
    public String validateSlots() {
        return BeanValidators.validateSlots(getSlots()) ? "groupverify?faces-redirect=true" : null;
    }

    /**
     * Validates the definitions
     */
    @Override
    public String validateDefinitions() {
        setStarted(true);
        return BeanValidators.validateDefinitions(getDefinitions()) ? "groupassignment?faces-redirect=true" : null;
    }

    /**
     * Called when the "Finish" button is clicked.
     * @return
     */
    public String finish() {
        String vDefinitions = validateDefinitions();
        String vSlots = validateSlots();
        String vMembers = validateMembers();
        String vIdentification = validateIdentification();

        if(vDefinitions == null || vSlots == null || vMembers == null || vIdentification == null) {
            return null;
        }

        try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
            Namespace namespace = mdr.get(ElementDAO.class).getNamespace(getNamespace());

            DataElementGroup group = new DataElementGroup();
            mdr.get(ElementDAO.class).saveElement(group);

            ScopedIdentifier superIdentifier = super.finish(group.getId(),
                    namespace.getId(), mdr);

            /**
             * In case the user has dropped elements from another namespace, use the importer which will return
             * the URN itself if it is already in the required namespace.
             */
            Importer importer = new Importer();

            for(MDRElement element : elements) {
                ScopedIdentifier sub = importer.importElement(mdr, element.getUrn(), superIdentifier.getNamespace(), getUserBean().getUserId());
                mdr.get(ScopedIdentifierDAO.class).addSubIdentifier(superIdentifier.getId(), sub.getId());
            }

            mdr.commit();

            refreshViews();

            return done(mdr);
        } catch (ValidationException | DuplicateIdentifierException e) {
            return null;
        } catch (DAOException e) {
            e.printStackTrace();
            Message.addError("serverError");
            return null;
        }
    }

    /**
     * Validates the members of this group.
     * @return
     */
    public String validateMembers() {
        return "groupslots?faces-redirect=true";
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public List<MDRElement> getElements() {
        return elements;
    }

    public void setElements(List<MDRElement> elements) {
        this.elements = elements;
    }

    @Override
    protected ElementType getElementType() {
        return ElementType.DATAELEMENTGROUP;
    }

    @Override
    public List<WizardStep> getSteps() {
        return Arrays.asList(new WizardStep(JSFUtil.getString("definitions"), 1, "/user/groupdefinitions.xhtml"),
                new WizardStep(JSFUtil.getString("membersShort"), 2, "/user/groupassignment.xhtml"),
                new WizardStep(JSFUtil.getString("slots"), 3, "/user/groupslots.xhtml"),
                new WizardStep(JSFUtil.getString("verification"), 4, "/user/groupverify.xhtml"));
    }

}
