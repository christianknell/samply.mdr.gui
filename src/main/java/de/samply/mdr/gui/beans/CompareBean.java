/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import de.samply.mdr.dao.IdentifiedDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.gui.ConnectionFactory;
import de.samply.mdr.gui.MDRElement;
import de.samply.mdr.util.Mapper;
import de.samply.sdao.DAOException;

/**
 * Compares at least two groups. Gets all elements in those groups first
 * and checks if the URNs describe the same elements.
 * @author paul
 *
 */
@ManagedBean
@ViewScoped
public class CompareBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ManagedProperty(value = "#{userBean}")
    private UserBean userBean;

    /**
     * The list of groups that should be compared.
     */
    private List<MDRElement> list;

    /**
     * The list of elements in those groups.
     */
    private List<MDRElement> elements;

    /**
     * A set of element IDs that have already been added to the list of elements.
     */
    private HashSet<Integer> addedElements;

    /**
     * The hashmap with the group as key and the set of element ids that are in this group as value..
     */
    private HashMap<MDRElement, HashSet<Integer>> rootMap;

    @PostConstruct
    public void initialize() {
        list = userBean.getCompare();

        elements = new ArrayList<>();
        addedElements = new HashSet<>();
        rootMap = new HashMap<>();

        try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
            for(MDRElement group : list) {
                rootMap.put(group, new HashSet<Integer>());
                addSubElements(rootMap.get(group), group, mdr);
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    /**
     * searches the group for all sub elements and adds them to the proper list / set
     * @param rootSet
     * @param group
     * @param mdr
     * @throws DAOException
     */
    private void addSubElements(HashSet<Integer> rootSet, MDRElement group, MDRConnection mdr) throws DAOException {
        for(MDRElement element : Mapper.convertElements(mdr.get(IdentifiedDAO.class).getAllSubMembers(group.getUrn()),
                userBean.getSelectedLanguage(), false)) {
            rootSet.add(element.getElement().getId());
            if(!addedElements.contains(element.getElement().getId())) {
                elements.add(element);
                addedElements.add(element.getElement().getId());
            }
        }
    }

    /**
     * Returns the bootstrap status (the color for the table row) for the given element
     * @param element
     * @return
     */
    public String status(MDRElement element) {
        int count = 0;

        for(MDRElement group : list) {
            if(contains(element, group)) {
                count++;
            }
        }

        if(count == 0) {
            return "danger";
        } else if(count == 1) {
            return "danger";
        } else if(count == list.size()) {
            return "success";
        } else {
            return "warning";
        }
    }

    public Boolean contains(MDRElement element, MDRElement group) {
        return rootMap.get(group).contains(element.getElement().getId());
    }

    /**
     * @return the userBean
     */
    public UserBean getUserBean() {
        return userBean;
    }

    /**
     * @param userBean the userBean to set
     */
    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    /**
     * @return the addedElements
     */
    public HashSet<Integer> getAddedElements() {
        return addedElements;
    }

    /**
     * @param addedElements the addedElements to set
     */
    public void setAddedElements(HashSet<Integer> addedElements) {
        this.addedElements = addedElements;
    }

    /**
     * @return the rootMap
     */
    public HashMap<MDRElement, HashSet<Integer>> getRootMap() {
        return rootMap;
    }

    /**
     * @param rootMap the rootMap to set
     */
    public void setRootMap(HashMap<MDRElement, HashSet<Integer>> rootMap) {
        this.rootMap = rootMap;
    }

    /**
     * @return the elements
     */
    public List<MDRElement> getElements() {
        return elements;
    }

    /**
     * @param elements the elements to set
     */
    public void setElements(List<MDRElement> elements) {
        this.elements = elements;
    }

    /**
     * @return the list
     */
    public List<MDRElement> getList() {
        return list;
    }

    /**
     * @param list the list to set
     */
    public void setList(List<MDRElement> list) {
        this.list = list;
    }

}
