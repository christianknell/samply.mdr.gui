/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui.beans;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import de.samply.mdr.dao.IdentifiedDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.NamespaceDAO;
import de.samply.mdr.gui.ConnectionFactory;
import de.samply.mdr.gui.MDRElement;
import de.samply.mdr.gui.MDRNamespace;
import de.samply.mdr.util.Mapper;
import de.samply.sdao.DAOException;
import de.samply.string.util.StringUtil;

/**
 * The bean that handles the view page.
 * @author paul
 *
 */
@ManagedBean
@ViewScoped
public class ViewBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ManagedProperty(value = "#{userBean}")
    private UserBean userBean;

    @ManagedProperty(value = "#{changeNamespaceWizard}")
    private ChangeNamespaceWizard wizard;

    /**
     * The namespaces that are relevant for the elements that have been found.
     */
    private List<MDRNamespace> namespaces = Collections.emptyList();

    /**
     * The list of the namespaces that the user has write access to.
     */
    private List<MDRNamespace> myNamespaces = Collections.emptyList();

    /**
     * The elements that are found for the selected namespace
     */
    private List<MDRElement> activeElements = Collections.emptyList();

    /**
     * The currently selected namespace as MDRNamespace
     */
    private MDRNamespace mdrNamespace = null;

    /**
     * The selected namespace.
     */
    private String selectedNamespace = "";

    /**
     * Called to initialize all fields.
     * Called by the the viewAction in xhtml files that require this bean.
     */
    public void initialize() {
        try(MDRConnection mdr = ConnectionFactory.get(userBean.getUserId())) {
            namespaces = Mapper.convertNamespaces(mdr.get(NamespaceDAO.class).getReadableNamespaces(), userBean.getSelectedLanguage());
            Collections.sort(namespaces, MDRNamespace.nameComparator);

            myNamespaces = getUserBean().getWritableNamespaces();
            Collections.sort(myNamespaces, MDRNamespace.nameComparator);

            for(MDRNamespace ns : myNamespaces) {
                namespaces.remove(ns);
            }

            if(!StringUtil.isEmpty(selectedNamespace)) {
                updateElements();
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    public void updateElements() {
        try (MDRConnection mdr = ConnectionFactory.get(userBean.getUserId())) {
            activeElements = Mapper.convertElements(mdr.get(IdentifiedDAO.class).getRootElements(selectedNamespace),
                    userBean.getSelectedLanguage(), true);
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Checks if this namespace is owned by the current user.
     * @return
     */
    public Boolean isOwnedNamespace() {
        return (mdrNamespace != null && mdrNamespace.getCreatedBy() == userBean.getUserId());
    }

    public void setActiveElements(List<MDRElement> elements) {
        this.activeElements = elements;
    }

    public List<MDRElement> getActiveElements() {
        return activeElements;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    /**
     * @return the wizard
     */
    public ChangeNamespaceWizard getWizard() {
        return wizard;
    }

    /**
     * @param wizard the wizard to set
     */
    public void setWizard(ChangeNamespaceWizard wizard) {
        this.wizard = wizard;
    }

    /**
     * @return the allNamespaces
     */
    public List<MDRNamespace> getNamespaces() {
        return namespaces;
    }

    /**
     * @param allNamespaces the allNamespaces to set
     */
    public void setNamespaces(List<MDRNamespace> allNamespaces) {
        this.namespaces = allNamespaces;
    }

    /**
     * @return the selectedNamespace
     */
    public String getSelectedNamespace() {
        return selectedNamespace;
    }

    /**
     * @param selectedNamespace the selectedNamespace to set
     */
    public void setSelectedNamespace(String selectedNamespace) {
        this.selectedNamespace = selectedNamespace;
    }

    /**
     * @return the myNamespaces
     */
    public List<MDRNamespace> getMyNamespaces() {
        return myNamespaces;
    }

    /**
     * @param myNamespaces the myNamespaces to set
     */
    public void setMyNamespaces(List<MDRNamespace> myNamespaces) {
        this.myNamespaces = myNamespaces;
    }

}
