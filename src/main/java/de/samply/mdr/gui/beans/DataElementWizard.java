/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui.beans;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import de.samply.mdr.dal.dto.CatalogValueDomain;
import de.samply.mdr.dal.dto.Code;
import de.samply.mdr.dal.dto.DataElement;
import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.DescribedElement;
import de.samply.mdr.dal.dto.ElementType;
import de.samply.mdr.dal.dto.EnumeratedValueDomain;
import de.samply.mdr.dal.dto.HierarchyNode;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.PermissibleCode;
import de.samply.mdr.dal.dto.PermissibleValue;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.dto.ValueDomain;
import de.samply.mdr.dao.DefinitionDAO;
import de.samply.mdr.dao.ElementDAO;
import de.samply.mdr.dao.IdentifiedDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.NamespaceDAO;
import de.samply.mdr.dao.PermissibleCodeDAO;
import de.samply.mdr.gui.BeanValidators;
import de.samply.mdr.gui.ConnectionFactory;
import de.samply.mdr.gui.MDRElement;
import de.samply.mdr.gui.ValueDomainDTO;
import de.samply.mdr.gui.WizardStep;
import de.samply.mdr.gui.beans.TreeNode.ChildrenSelectionState;
import de.samply.mdr.gui.exceptions.DuplicateIdentifierException;
import de.samply.mdr.gui.exceptions.ValidationException;
import de.samply.mdr.lib.AdapterDefinition;
import de.samply.mdr.lib.AdapterElement;
import de.samply.mdr.lib.AdapterPermissibleValue;
import de.samply.mdr.lib.AdapterValueDomain;
import de.samply.mdr.lib.Constants.DatatypeField;
import de.samply.mdr.util.JSFUtil;
import de.samply.mdr.util.Mapper;
import de.samply.mdr.util.Message;
import de.samply.sdao.DAOException;
import de.samply.sdao.json.JSONResource;

/**
 * The wizard used to create a new data element (or use an existing
 * data element as a template) or change a data element
 * in the first draft status.
 * @author paul
 *
 */
@ManagedBean
@SessionScoped
public class DataElementWizard extends ElementWizard {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The data elements value domain.
     */
    private ValueDomainDTO valueDomain;

    /**
     * The list of available catalogs.
     */
    private List<MDRElement> availableCatalogs;

    /**
     * The currently selected code. This is only necessary for dataelements with a catalog value domain.
     */
    private String selectedCode;

    /**
     * The external adapter element used as a template for this data element wizard.
     */
    private AdapterElement externalElement = null;

    /**
     * Adds another permitted value to the value domain.
     * This is called when the user presses the "add another permitted value"
     * button.
     */
    public String addPermittedValue() {
        AdapterPermissibleValue value = new AdapterPermissibleValue();
        for(AdapterDefinition definition : getDefinitions()) {
            AdapterDefinition def = new AdapterDefinition();
            def.setLanguage(definition.getLanguage());
            value.getDefinitions().add(def);
        }
        valueDomain.getPermittedValues().add(value);
        return null;
    }

    /**
     * Removes the given permissible value
     * @param value
     */
    public void remove(AdapterPermissibleValue value) {
        valueDomain.getPermittedValues().remove(value);
    }

    @Override
    public String template(String urn) {
        try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
            template(urn, mdr);
            valueDomain = Mapper.createValueDomain(getTemplateElement().getElement(), mdr, getUserBean().getSelectedLanguage());
        } catch (DAOException e) {
            reset();
            return null;
        }

        return "/user/elementdefinitions?faces-redirect=true";
    }

    public String initialize(AdapterElement externalTemplate) {
        reset();

        externalElement = externalTemplate;

        AdapterValueDomain externalVD = externalTemplate.createValueDomain();

        valueDomain.setPermittedValues(externalVD.getPermittedValues());

        valueDomain.setDatatype(externalVD.getDatatype());

        valueDomain.setFormat(externalVD.getFormat());
        valueDomain.setUseRegex(externalVD.getUseRegex());
        valueDomain.setUseRangeFrom(externalVD.getUseRangeFrom());
        valueDomain.setUseRangeTo(externalVD.getUseRangeTo());
        valueDomain.setRangeFrom(externalVD.getRangeFrom());
        valueDomain.setRangeTo(externalVD.getRangeTo());

        valueDomain.setDateRepresentation(externalVD.getDateRepresentation());
        valueDomain.setTimeRepresentation(externalVD.getTimeRepresentation());
        valueDomain.setUnitOfMeasure(externalVD.getUnitOfMeasure());
        valueDomain.setUseMaxLength(externalVD.getUseMaxLength());
        valueDomain.setMaxLength(externalVD.getMaxLength());
        valueDomain.setWithinRange(externalVD.getWithinRange());
        valueDomain.setWithDays(externalVD.getWithDays());
        valueDomain.setWithSeconds(externalVD.getWithSeconds());

        setDefinitions(externalTemplate.getDefinitions());

        return "/user/elementdefinitions?faces-redirect=true";
    }

    /**
     * Validates the value domain.
     * @return
     */
    public String validateValidation() {
        boolean valid = BeanValidators.validateValidation(valueDomain);
        if(valid == false) {
            return null;
        } else if(valueDomain.getDatatype() == DatatypeField.CATALOG) {

            if(valueDomain.getCatalogRoot() == null || !valueDomain.getCatalogRoot().getElement().getUrn().equals(valueDomain.getCatalog())) {
                try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
                    IdentifiedElement catalog = mdr.get(IdentifiedDAO.class).getElement(valueDomain.getCatalog());

                    valueDomain.setCatalogRoot(new TreeNode(Mapper.convert(catalog, getUserBean().getSelectedLanguage())));

                    HashMap<Integer, TreeNode> index = new HashMap<>();
                    index.put(catalog.getScoped().getId(), valueDomain.getCatalogRoot());

                    HashMap<String, TreeNode> urnIndex = new HashMap<>();
                    urnIndex.put(valueDomain.getCatalog(), valueDomain.getCatalogRoot());

                    for(IdentifiedElement element : mdr.get(IdentifiedDAO.class).getAllSubMembers(valueDomain.getCatalog())) {
                        index.put(element.getScoped().getId(), new TreeNode(Mapper.convert(element, getUserBean().getSelectedLanguage())));
                        urnIndex.put(element.getScoped().toString(), index.get(element.getScoped().getId()));
                    }

                    valueDomain.setSubCodes(Mapper.convertElements(mdr.get(IdentifiedDAO.class).getEntries(valueDomain.getCatalog()),
                            getUserBean().getSelectedLanguage(), false));

                    for(HierarchyNode node : mdr.getHierarchyNodes(valueDomain.getCatalog())) {
                        index.get(node.getSuperId()).getChildren().add(index.get(node.getSubId()));
                        index.get(node.getSubId()).getParents().add(index.get(node.getSuperId()));
                    }

                    valueDomain.setUrnIndex(urnIndex);
                    valueDomain.setIndex(index);
                } catch (DAOException e) {
                    e.printStackTrace();
                }
            }

            return "elementcatalog?faces-redirect=true";
        } else {
            return "elementslots?faces-redirect=true";
        }
    }

    @Override
    public String reset() {
        externalElement = null;
        valueDomain = new ValueDomainDTO();

        try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
            availableCatalogs = Mapper.convertElements(mdr.get(IdentifiedDAO.class).getCatalogs(),
                    getUserBean().getSelectedLanguage(), true);
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return super.reset();
    }

    /**
     * Called when the user finished the wizard. This method must save the
     * data element in the database, if everything is valid.
     * @return
     */
    public String finish() {
        String vDefinitions = validateDefinitions();
        String vSlots = validateSlots();
        String vValidation = validateValidation();
        String vIdentification = validateIdentification();

        if(vDefinitions == null || vSlots == null || vValidation == null || vIdentification == null) {
            return null;
        }

        try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
            DescribedElement namespace = mdr.get(NamespaceDAO.class).getNamespace(getNamespace());
            Namespace ns = (Namespace) namespace.getElement();

            ValueDomain domain = Mapper.convert(valueDomain);

            /**
             * Determine the maxLength of all available permitted values.
             */
            if(domain instanceof EnumeratedValueDomain) {
                int maxLength = 1;
                for(AdapterPermissibleValue dto : valueDomain.getPermittedValues()) {
                    if(maxLength < dto.getValue().length()) {
                        maxLength = dto.getValue().length();
                    }
                }
                domain.setMaxCharacters(maxLength);
            }

            /**
             * Determine the maxLength value for all currently available codes.
             */
            if(domain instanceof CatalogValueDomain) {
                int maxLength = 1;
                for(TreeNode node : valueDomain.getIndex().values()) {
                    if(node.getElement().getElement().getElementType() == ElementType.CODE) {
                        Code c = (Code) node.getElement().getElement().getElement();
                        int length = c.getCode().length();
                        if(node.isSelected() && length > maxLength) {
                            maxLength = length;
                        }
                    }
                }
                domain.setMaxCharacters(maxLength);
            }

            /**
             * Save the value domain first.
             */
            mdr.get(ElementDAO.class).saveElement(domain);

            DataElement element = new DataElement();
            element.setValueDomainId(domain.getId());

            if(externalElement != null) {
                element.setExternalId(externalElement.getIdentification());
            }

            mdr.get(ElementDAO.class).saveElement(element);

            ScopedIdentifier identifier = super.finish(element.getId(), namespace.getElement().getId(), mdr);

            JSONResource constraints = ns.getConstraints();

            if(domain instanceof EnumeratedValueDomain) {
                /**
                 * For every value we have to check the namespace constraints.
                 */
                boolean valid = true;

                for(AdapterPermissibleValue v : valueDomain.getPermittedValues()) {
                    PermissibleValue value = new PermissibleValue();
                    value.setValueDomainId(domain.getId());
                    value.setPermittedValue(v.getValue());

                    mdr.get(ElementDAO.class).saveElement(value);

                    valid = valid && BeanValidators.validateValueConstraints(constraints, v);

                    for(AdapterDefinition definition : v.getDefinitions()) {
                        Definition def = Mapper.convert(definition);
                        def.setElementId(value.getId());
                        def.setScopedIdentifierId(identifier.getId());
                        mdr.get(DefinitionDAO.class).saveDefinition(def);
                    }
                }

                /**
                 * If the namespace constraints are not met, throw an exception to show the error messages.
                 */
                if(!valid) {
                    throw new ValidationException();
                }
            }

            if(domain instanceof CatalogValueDomain) {
                PermissibleCodeDAO dao = mdr.get(PermissibleCodeDAO.class);
                for(TreeNode node : valueDomain.getIndex().values()) {
                    if(node.getState() != ChildrenSelectionState.NONE && node.getElement().getElement().getElementType() == ElementType.CODE) {
                        PermissibleCode code = new PermissibleCode();
                        code.setCatalogValueDomainId(domain.getId());
                        code.setCodeScopedIdentifierId(node.getElement().getElement().getScoped().getId());
                        dao.savePermissibleCode(code);
                    }
                }
            }

            mdr.commit();

            return done(mdr);
        } catch (ValidationException | DuplicateIdentifierException e) {
            return null;
        } catch (DAOException e) {
            e.printStackTrace();
            Message.addError("serverError");
            return null;
        }
    }

    /**
     * Selects all nodes.
     * @return
     */
    public String selectAll() {
        selectAll(valueDomain.getUrnIndex().get(selectedCode));
        valueDomain.updateTreeState();
        return null;
    }

    /**
     * Selects all nodes in the given tree node.
     * @param node
     */
    private void selectAll(TreeNode node) {
        node.setSelected(true);
        node.setState(ChildrenSelectionState.ALL);

        for(TreeNode child : node.getChildren()) {
            selectAll(child);
        }
    }

    /**
     * Deselects all nodes.
     * @return
     */
    public String deselectAll() {
        deselectAll(valueDomain.getUrnIndex().get(selectedCode));
        valueDomain.updateTreeState();
        return null;
    }

    /**
     * Deselects all codes in the given tree node.
     * @param node
     */
    private void deselectAll(TreeNode node) {
        node.setSelected(false);
        node.setState(ChildrenSelectionState.NONE);

        for(TreeNode child : node.getChildren()) {
            child.setSelected(false);

            if(child.getState() != ChildrenSelectionState.NONE) {
                child.setState(ChildrenSelectionState.NONE);
                deselectAll(child);
            }
        }
    }

    public String validateCatalog() {
        return "elementslots?faces-redirect=true";
    }

    @Override
    public String validateSlots() {
        return BeanValidators.validateSlots(getSlots()) ? "elementverify?faces-redirect=true" : null;
    }

    @Override
    public String validateDefinitions() {
        setStarted(true);
        if(BeanValidators.validateDefinitions(getDefinitions())) {
            if(valueDomain.getPermittedValues().size() == 0) {
                addPermittedValue();
            }
            return "validation?faces-redirect=true";
        } else {
            return null;
        }
    }

    @Override
    public List<WizardStep> getSteps() {
        return Arrays.asList(new WizardStep(JSFUtil.getString("definitions"), 1, "/user/elementdefinitions.xhtml"),
                new WizardStep(JSFUtil.getString("validation"), 2, "/user/validation.xhtml", "/user/elementcatalog.xhtml"),
                new WizardStep(JSFUtil.getString("slots"), 3, "/user/elementslots.xhtml"),
                new WizardStep(JSFUtil.getString("verification"), 4, "/user/elementverify.xhtml"));
    }

    @Override
    protected ElementType getElementType() {
        return ElementType.DATAELEMENT;
    }

    public ValueDomainDTO getValueDomain() {
        return valueDomain;
    }

    public void setValueDomain(ValueDomainDTO valueDomain) {
        this.valueDomain = valueDomain;
    }

    /**
     * @return the availableCatalogs
     */
    public List<MDRElement> getAvailableCatalogs() {
        return availableCatalogs;
    }

    /**
     * @param availableCatalogs the availableCatalogs to set
     */
    public void setAvailableCatalogs(List<MDRElement> availableCatalogs) {
        this.availableCatalogs = availableCatalogs;
    }

    /**
     * @return the selectedCode
     */
    public String getSelectedCode() {
        return selectedCode;
    }

    /**
     * @param selectedCode the selectedCode to set
     */
    public void setSelectedCode(String selectedCode) {
        this.selectedCode = selectedCode;
    }

    /**
     * @return the externalElement
     */
    public AdapterElement getExternalElement() {
        return externalElement;
    }

    /**
     * @param externalElement the externalElement to set
     */
    public void setExternalElement(AdapterElement externalElement) {
        this.externalElement = externalElement;
    }

}
