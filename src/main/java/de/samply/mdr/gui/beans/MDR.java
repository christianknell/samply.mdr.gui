/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui.beans;

import java.io.Serializable;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import de.samply.mdr.dal.dto.NamespacePermission.Permission;
import de.samply.mdr.lib.Constants.DatatypeField;
import de.samply.mdr.lib.Constants.DateRepresentation;
import de.samply.mdr.lib.Constants.Language;
import de.samply.mdr.lib.Constants.Source;
import de.samply.mdr.lib.Constants.TimeRepresentation;


/**
 * Application scoped bean that provides methods to get enums.
 * @author paul
 *
 */
@ApplicationScoped
@ManagedBean(name = "mdr")  // do not remove this name... otherwise it would be "mDR".
public class MDR implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Returns all available permissions
     * @return
     */
    public Permission[] getPermissions() {
        return Permission.values();
    }

    /**
     * Returns all available datatypes
     * @return
     */
    public DatatypeField[] getDatatypeFields() {
        return DatatypeField.values();
    }

    /**
     * Returns all available languages
     * @return
     */
    public Language[] getLanguages() {
        return Language.values();
    }

    /**
     * Returns all available date representation formats
     * @return
     */
    public DateRepresentation[] getDateRepresentations() {
        return DateRepresentation.values();
    }

    /**
     * Returns all available time representation formats
     * @return
     */
    public TimeRepresentation[] getTimeRepresentations() {
        return TimeRepresentation.values();
    }

    /**
     * Returns all available sources
     * @return
     */
    public Source[] getSources() {
        return Source.values();
    }

}
