/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui.beans;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.Part;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.DescribedElement;
import de.samply.mdr.dal.dto.ElementType;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.utils.Importer;
import de.samply.mdr.dao.utils.Importer.Import;
import de.samply.mdr.dao.utils.Importer.ImportStrategy;
import de.samply.mdr.gui.ConnectionFactory;
import de.samply.mdr.gui.MDRNamespace;
import de.samply.mdr.gui.WizardStep;
import de.samply.mdr.util.JSFUtil;
import de.samply.mdr.util.Message;
import de.samply.mdr.xsd.Catalog;
import de.samply.mdr.xsd.Element;
import de.samply.mdr.xsd.Export;
import de.samply.mdr.xsd.Namespace;
import de.samply.mdr.xsd.ObjectFactory;
import de.samply.sdao.DAOException;
import de.samply.sdao.json.JSONResource;

/**
 * This wizard is used to create **and change existing catalogs**.
 */
@ManagedBean
@SessionScoped
public class ImportWizard extends AbstractWizard {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The steps of this wizard
     */
    private List<WizardStep> steps;

    /**
     * The uploaded file, must be a valid catalog XML file.
     */
    private transient Part file;

    /**
     * The deserialized uploaded export object
     */
    private Export export;

    /**
     * The current ImportStrategys
     */
    private List<ImportStrategy> strategy = Collections.emptyList();

    /**
     * The list of target namespaces. Those namespaces must be writable.
     */
    private List<MDRNamespace> targetNamespaces = Collections.emptyList();

    @ManagedProperty(value = "#{userBean}")
    private UserBean userBean;

    private List<Import> imports = Collections.emptyList();

    public ImportWizard() {
        steps = new ArrayList<>();
        steps.add(new WizardStep(JSFUtil.getString("upload"), 1, "/user/import.xhtml"));
        steps.add(new WizardStep(JSFUtil.getString("strategy"), 2, "/user/importstrategy.xhtml"));
        steps.add(new WizardStep(JSFUtil.getString("verification"), 3, "/user/importverify.xhtml"));
    }

    /**
     * @param urn
     * @return
     */
    public String initialize() {
        return steps.get(0).getHref().get(0);
    }

    /**
     * Saves the catalog in the database.
     * @return
     */
    public String finish() {
        return null;
    }

    /**
     * This method is called right after the upload. It checks if the uploaded catalog is valid (it ises the XSD file
     * to do that).
     * @return
     */
    public String upload() {
        try {
            JAXBContext context = JAXBContext.newInstance(ObjectFactory.class);

            InputStream commonXSD = Catalog.class.getClassLoader().getResourceAsStream("common.xsd");
            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(new StreamSource(commonXSD));

            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(file.getInputStream()));

            Object element = context.createUnmarshaller().unmarshal(file.getInputStream());

            if(!(element instanceof Export)) {
                Message.addError("invalidImportFile");
                return null;
            }

            export = (Export) element;

            strategy = new ArrayList<>();

            for(JAXBElement<? extends Element> jaxb : export.getElement()) {
                if(jaxb.getValue() instanceof de.samply.mdr.xsd.Namespace) {
                    de.samply.mdr.xsd.Namespace namespace = (Namespace) jaxb.getValue();
                    ImportStrategy is = new ImportStrategy();
                    is.setFrom(namespace.getName());

                    strategy.add(is);
                }
            }

            targetNamespaces = new ArrayList<>(userBean.getWritableNamespaces());

            return "/user/importstrategy.xhtml?faces-redirect=true";
        } catch (IOException | JAXBException e) {
            e.printStackTrace();
            Message.addError("invalidCatalog");
            return null;
        } catch (SAXException e) {
            e.printStackTrace();
            Message.addError("invalidCatalogError", e.getMessage());
            return null;
        }
    }

    public String verify() {
        try(MDRConnection mdr = ConnectionFactory.get(userBean.getUserId())) {
            Importer importer = new Importer();
            importer.importExport(export, strategy, mdr);
            imports  = importer.getImports();
            return "/user/importverify.xhtml";
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String commit() {
        try(MDRConnection mdr = ConnectionFactory.get(userBean.getUserId())) {
            Importer importer = new Importer();
            importer.importExport(export, strategy, mdr);
            mdr.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return "/index?faces-redirect=true";
    }

    public MDRNamespace createNewTargetNamespace(String name, String label, int id) {
        DescribedElement newNamespace = new DescribedElement();

        de.samply.mdr.dal.dto.Namespace namespace = new de.samply.mdr.dal.dto.Namespace();
        namespace.setCreatedBy(0);
        namespace.setData(new JSONResource());
        namespace.setElementType(ElementType.NAMESPACE);
        namespace.setHidden(false);
        namespace.setId(0);
        namespace.setName(name);
        namespace.setUuid(UUID.randomUUID());

        List<Definition> definitions = new ArrayList<>();
        Definition def = new Definition();
        def.setDefinition(label);
        def.setDesignation(label);
        def.setLanguage("en");
        definitions.add(def);
        newNamespace.setDefinitions(definitions);
        newNamespace.setElement(namespace);
        return new MDRNamespace(newNamespace, userBean.getSelectedLanguage());
    }

    public String reset() {
        file = null;
        export = null;

        return "/index.xhtml?faces-direct=true";
    }

    /* (non-Javadoc)
     * @see de.samply.mdr.gui.beans.AbstractWizard#getReleaseDraftButtons()
     */
    @Override
    public Boolean getReleaseDraftButtons() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public List<WizardStep> getSteps() {
        return steps;
    }

    /**
     * @return the file
     */
    public Part getFile() {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(Part file) {
        this.file = file;
    }

    /**
     * @return the userBean
     */
    public UserBean getUserBean() {
        return userBean;
    }

    /**
     * @param userBean the userBean to set
     */
    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    /* (non-Javadoc)
     * @see de.samply.mdr.gui.beans.AbstractWizard#getDesignation()
     */
    @Override
    public String getDesignation() {
        return "Upload file";
    }

    /**
     * @return the export
     */
    public Export getExport() {
        return export;
    }

    /**
     * @param export the export to set
     */
    public void setExport(Export export) {
        this.export = export;
    }

    /**
     * @return the strategy
     */
    public List<ImportStrategy> getStrategy() {
        return strategy;
    }

    /**
     * @param strategy the strategy to set
     */
    public void setStrategy(List<ImportStrategy> strategy) {
        this.strategy = strategy;
    }

    /**
     * @return the targetNamespaces
     */
    public List<MDRNamespace> getTargetNamespaces() {
        return targetNamespaces;
    }

    /**
     * @param targetNamespaces the targetNamespaces to set
     */
    public void setTargetNamespaces(List<MDRNamespace> targetNamespaces) {
        this.targetNamespaces = targetNamespaces;
    }

    /**
     * @return the imports
     */
    public List<Import> getImports() {
        return imports;
    }

    /**
     * @param imports the imports to set
     */
    public void setImports(List<Import> imports) {
        this.imports = imports;
    }

}
