/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui.beans;

import java.util.Arrays;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import de.samply.mdr.dal.dto.ElementType;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.Record;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dao.ElementDAO;
import de.samply.mdr.dao.IdentifiedDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.ScopedIdentifierDAO;
import de.samply.mdr.dao.utils.Importer;
import de.samply.mdr.gui.BeanValidators;
import de.samply.mdr.gui.ConnectionFactory;
import de.samply.mdr.gui.MDRElement;
import de.samply.mdr.gui.WizardStep;
import de.samply.mdr.gui.exceptions.DuplicateIdentifierException;
import de.samply.mdr.gui.exceptions.ValidationException;
import de.samply.mdr.util.JSFUtil;
import de.samply.mdr.util.Mapper;
import de.samply.mdr.util.Message;
import de.samply.sdao.DAOException;

/**
 * The wizard used to create a new record (or use an existing
 * record as a template) or change a record in the first draft status.
 * @author paul
 *
 */
@ManagedBean
@SessionScoped
public class RecordWizard extends DataElementGroupWizard {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Override
    public String validateMembers() {
        return "/user/recordslots?faces-redirect=true";
    }

    @Override
    public String validateSlots() {
        return BeanValidators.validateSlots(getSlots()) ? "/user/recordverify?faces-redirect=true" : null;
    }

    @Override
    public String validateDefinitions() {
        setStarted(true);
        return BeanValidators.validateDefinitions(getDefinitions()) ? "/user/recordassignment?faces-redirect=true" : null;
    }

    @Override
    public String template(String urn) {
        try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
            template(urn, mdr);
            setElements(Mapper.convertElements(mdr.get(IdentifiedDAO.class).getEntries(urn),
                    getUserBean().getSelectedLanguage(), false));
        } catch (DAOException e) {
            reset();
            return null;
        }

        return "/user/recorddefinitions?faces-redirect=true";
    }

    @Override
    protected boolean elementValid(MDRElement foundElement) {
        /**
         * Records can contain dataelements only.
         */
        return foundElement.getElement().getElementType() == ElementType.DATAELEMENT;
    }

    @Override
    public String finish() {
        String vDefinitions = validateDefinitions();
        String vSlots = validateSlots();
        String vMembers = validateMembers();
        String vIdentification = validateIdentification();

        if(vDefinitions == null || vSlots == null || vMembers == null || vIdentification == null) {
            return null;
        }

        try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
            Namespace namespace = mdr.get(ElementDAO.class).getNamespace(getNamespace());

            Record record = new Record();
            mdr.get(ElementDAO.class).saveElement(record);

            ScopedIdentifier superIdentifier = super.finish(record.getId(),
                    namespace.getId(), mdr);

            Importer importer = new Importer();

            int order = 1;
            for(MDRElement element : getElements()) {
                ScopedIdentifier sub = importer.importElement(mdr, element.getUrn(), namespace.getName(), getUserBean().getUserId());
                mdr.get(ScopedIdentifierDAO.class).addSubIdentifier(superIdentifier.getId(), sub.getId(), order ++);
            }

            mdr.commit();

            refreshViews();

            return done(mdr);
        } catch (ValidationException | DuplicateIdentifierException e) {
            return null;
        } catch (DAOException e) {
            e.printStackTrace();
            Message.addError("serverError");
            return null;
        }
    }

    @Override
    protected ElementType getElementType() {
        return ElementType.RECORD;
    }

    @Override
    public List<WizardStep> getSteps() {
        return Arrays.asList(new WizardStep(JSFUtil.getString("definitions"), 1, "/user/recorddefinitions.xhtml"),
                new WizardStep(JSFUtil.getString("entries"), 2, "/user/recordassignment.xhtml"),
                new WizardStep(JSFUtil.getString("slots"), 3, "/user/recordslots.xhtml"),
                new WizardStep(JSFUtil.getString("verification"), 4, "/user/recordverify.xhtml"));
    }

}
