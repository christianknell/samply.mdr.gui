/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui.beans;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import de.samply.mdr.dal.dto.Code;
import de.samply.mdr.dal.dto.ElementType;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dao.IdentifiedDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.ScopedIdentifierDAO;
import de.samply.mdr.dao.SlotDAO;
import de.samply.mdr.gui.ConnectionFactory;
import de.samply.mdr.gui.DetailedMDRElement;
import de.samply.mdr.gui.MDRElement;
import de.samply.mdr.util.Mapper;
import de.samply.sdao.DAOException;

/**
 * Handles the detailed view of an element
 * @author paul
 *
 */
@ViewScoped
@ManagedBean
public class DetailViewBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The URN of the element
     */
    private String urn;

    /**
     * If true, the members are draggable
     */
    private Boolean draggable;

    /**
     * The fully detailed MDR element.
     */
    private DetailedMDRElement element;

    @ManagedProperty(value = "#{userBean}")
    private UserBean userBean;

    @ManagedProperty(value = "#{dataElementWizard}")
    private DataElementWizard dataElementWizard;

    @ManagedProperty(value = "#{dataElementGroupWizard}")
    private DataElementGroupWizard dataElementGroupWizard;

    @ManagedProperty(value = "#{recordWizard}")
    private RecordWizard recordWizard;

    @ManagedProperty(value = "#{catalogWizard}")
    private CatalogWizard catalogWizard;

    @ManagedProperty(value = "#{changeWizard}")
    private ChangeWizard changeWizard;

    private String importIntoNamespace;

    private Boolean isValid;

    private String node;

    private List<MDRElement> codes;

    private boolean readonly;

    /**
     * Called by the f:viewAction
     */
    public void initialize() {
        try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
            element = Mapper.convert(mdr.get(IdentifiedDAO.class).getElement(urn), Mapper.convertSlots(mdr.get(SlotDAO.class).getSlots(urn)),
                    mdr.get(ScopedIdentifierDAO.class).getVersions(urn), mdr, getUserBean().getSelectedLanguage());

            if(element.getElement().getElementType() == ElementType.CODE) {
                Code code = (Code) element.getElement().getElement();
                isValid = code.isValid();
            }

            if(node != null) {
                setCodes(Mapper.convertElements(mdr.get(IdentifiedDAO.class).getEntries(node), getUserBean().getSelectedLanguage(), false));
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Resets the corresponding wizard and starts it with the specified element
     * as template.
     * @param element
     * @return
     */
    public String template(MDRElement element) {
        ElementWizard wiz = getWizard(element);
        wiz.reset();
        return wiz.template(element.getUrn());
    }

    /**
     * returns the correct wizard for the specified element
     * @param element
     * @return
     */
    private ElementWizard getWizard(MDRElement element) {
        switch(element.getElement().getElementType()) {
        case DATAELEMENT:
            return getDataElementWizard();

        case DATAELEMENTGROUP:
            return getDataElementGroupWizard();

        case RECORD:
            return getRecordWizard();

        case CATALOG:
            return getCatalogWizard();

        default:
            return null;
        }
    }

    /**
     * initializes the correct wizard and redirects the user to the
     * correct web page
     * @param element
     * @return
     */
    public String change(MDRElement element) {
        if(element.firstDraft()) {
            getWizard(element).setUseAsTemplate(false);
            return getWizard(element).template(element.getUrn());
        } else {
            /**
             * Because changing catalogs is treated differently and it uses
             * a different wizard.
             */
            if(element.getElement().getElementType() == ElementType.CATALOG) {
                getCatalogWizard().setUseAsTemplate(false);
                return getCatalogWizard().initialize(element.getUrn());
            } else {
                getChangeWizard().setUseAsTemplate(false);
                return getChangeWizard().initialize(element.getUrn());
            }
        }
    }

    /**
     * Releases the element. It outdates all other released scoped identifiers for
     * this scoped identifier.
     * @param element
     * @return
     */
    public String release(MDRElement element) {
        if(element.getDraft() && getUserBean().isWritableNamespaceURN(element.getUrn())) {
            try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
                mdr.get(ScopedIdentifierDAO.class).outdateIdentifiers(element.getUrn());
                mdr.get(ScopedIdentifierDAO.class).release(element.getElement().getScoped());
                mdr.commit();
            } catch (DAOException e) {
                e.printStackTrace();
            }
        }

        return "/detail?faces-redirect=true&urn=" + element.getUrn();
    }

    /**
     * Duplicates an element.
     * @param element
     * @return
     */
    public String duplicate(MDRElement element) {
        try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
            ScopedIdentifier duplicate = mdr.duplicate(element.getUrn(),
                    element.getElement().getScoped().getNamespace(), userBean.getUserId(), true);
            mdr.commit();

            return "/detail?faces-redirect=true&urn=" + duplicate.toString();
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public DetailedMDRElement getElement() {
        return element;
    }

    public void setElement(DetailedMDRElement element) {
        this.element = element;
    }

    public DataElementWizard getDataElementWizard() {
        return dataElementWizard;
    }

    public void setDataElementWizard(DataElementWizard dataElementWizard) {
        this.dataElementWizard = dataElementWizard;
    }

    public DataElementGroupWizard getDataElementGroupWizard() {
        return dataElementGroupWizard;
    }

    public void setDataElementGroupWizard(DataElementGroupWizard dataElementGroupWizard) {
        this.dataElementGroupWizard = dataElementGroupWizard;
    }

    public RecordWizard getRecordWizard() {
        return recordWizard;
    }

    public void setRecordWizard(RecordWizard recordWizard) {
        this.recordWizard = recordWizard;
    }

    public Boolean getDraggable() {
        return draggable;
    }

    public void setDraggable(Boolean draggable) {
        this.draggable = draggable;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public String getImportIntoNamespace() {
        return importIntoNamespace;
    }

    public void setImportIntoNamespace(String importIntoNamespace) {
        this.importIntoNamespace = importIntoNamespace;
    }

    public ChangeWizard getChangeWizard() {
        return changeWizard;
    }

    public void setChangeWizard(ChangeWizard changeWizard) {
        this.changeWizard = changeWizard;
    }

    public String getUrn() {
        return urn;
    }

    public void setUrn(String urn) {
        this.urn = urn;
    }

    /**
     * @return the isValid
     */
    public Boolean getIsValid() {
        return isValid;
    }

    /**
     * @param isValid the isValid to set
     */
    public void setIsValid(Boolean isValid) {
        this.isValid = isValid;
    }

    /**
     * @return the node
     */
    public String getNode() {
        return node;
    }

    /**
     * @param node the node to set
     */
    public void setNode(String node) {
        this.node = node;
    }

    /**
     * @return the codes
     */
    public List<MDRElement> getCodes() {
        return codes;
    }

    /**
     * @param codes the codes to set
     */
    public void setCodes(List<MDRElement> codes) {
        this.codes = codes;
    }

    /**
     * @return the readonly
     */
    public boolean isReadonly() {
        return readonly;
    }

    /**
     * @param readonly the readonly to set
     */
    public void setReadonly(boolean readonly) {
        this.readonly = readonly;
    }

    /**
     * @return the catalogWizard
     */
    public CatalogWizard getCatalogWizard() {
        return catalogWizard;
    }

    /**
     * @param catalogWizard the catalogWizard to set
     */
    public void setCatalogWizard(CatalogWizard catalogWizard) {
        this.catalogWizard = catalogWizard;
    }

}
