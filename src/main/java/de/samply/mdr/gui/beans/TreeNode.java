/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.samply.mdr.gui.MDRElement;

/**
 * TreeNodes are used for catalogs. They have a state for itself, the state
 * of all children, and a reference to the parents and children of this node.
 */
public class TreeNode implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * <pre>
     * ALL: the code itself and *all* children are selected
     * SOME: the code itself or at least one child is selected
     * NONE: the code itself nor any child is selected
     * </pre>
     */
    public enum ChildrenSelectionState {
        ALL,
        SOME,
        NONE};

    /**
     * If true, this node itself is selected as valid code.
     */
    private boolean isSelected = true;

    /**
     * The state of this node.
     */
    private TreeNode.ChildrenSelectionState state = ChildrenSelectionState.ALL;

    /**
     * The MDR element (Code) that this node describes.
     */
    private final MDRElement element;

    /**
     * The parents of this node.
     */
    private final List<TreeNode> parents = new ArrayList<>();

    /**
     * The children of this node.
     */
    private final List<TreeNode> children = new ArrayList<>();

    public TreeNode(MDRElement element) {
        this.element = element;
    }

    /**
     * @return the element
     */
    public MDRElement getElement() {
        return element;
    }

    /**
     * @return the children
     */
    public List<TreeNode> getChildren() {
        return children;
    }

    /**
     * @return the parents
     */
    public List<TreeNode> getParents() {
        return parents;
    }

    /**
     * @return the isSelected
     */
    public boolean isSelected() {
        return isSelected;
    }

    /**
     * @param isSelected the isSelected to set
     */
    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    /**
     * @return the state
     */
    public TreeNode.ChildrenSelectionState getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(TreeNode.ChildrenSelectionState state) {
        this.state = state;
    }
}
