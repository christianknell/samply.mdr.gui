#
# Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
# Contact: info@osse-register.de
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation; either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, see <http://www.gnu.org/licenses>.
#
# Additional permission under GNU GPL version 3 section 7:
#
# If you modify this Program, or any covered work, by linking or combining it
# with Jersey (https://jersey.java.net) (or a modified version of that
# library), containing parts covered by the terms of the General Public
# License, version 2.0, the licensors of this Program grant you additional
# permission to convey the resulting work.
#

projectName = MDR
samply = Samply

home = Home
createNew = New
newElement = New Dataelement
newGroup = New Dataelementgroup
newRecord = New Record
newNamespace = New Namespace
uploadCatalog = Upload catalog
myDrafts = My drafts
source = Source
myNamespaces = My namespaces
search = Search
compare = Compare
browse = Browse
view = View
starred = Starred
export = Export
importFile = Import file

oldIdentifier = Old identifier
newIdentifier = New identifier

namespaces = Namespaces
users = Users
config = Config

currentConfig = Current Configuration
saveConfig = Save config
newUserNamespaceGrant = Grant new users permission to create new namespaces
newUserCatalogGrant = Grant new users permission to upload new catalogs
newUserExportImportGrant = Grant new users permission to export and import elements
showTranslationButton = Show the button on definitions to use an external translation service

constraints = Constraints
myNamespaces = My namespaces
allNamespaces = All namespaces
otherNamespaces = Other namespaces
showHideNamespaces = Show / Hide Namespaces
filter = Filter

comparison = Comparison
profile = Profile
login = Login with your Samply Account
username = Username

otherRepesentations = Other representations
noOtherRepresentations = No other representations available

strategy = Strategy
definitions = Definitions
validation = Validation
slots = Slots
verification = Verification
identification = Identification
permissions = Permissions
upload = Upload catalog
uploadFile = Upload file
uploadCatalogFile = Upload catalog file
uploadStrategy = Import strategy
externalData = External data
applyDataAbove = Apply the data above as accurately as possible

definition = Definition
definitionPlaceholder = Enter here the definition

designation = Designation
designationPlaceholder = Enter here a short designation

requiredSlots = Required Slots
requiredLanguages = Required Languages
showAllLanguages = Show all languages
importIntoNamespace = Import into my namespace
selectNamespace = Select a namespace
namespace = Namespace
language = Language
remove = Remove
add = Add
addPermittedValue = Add another permitted value
addDefinition = Add another definition
addSlot = Add another slot
addNewPermission = Add new permission

noSearchResults = This search request did not return any results.
namespaceEmpty = This namespace is currently empty.
noOtherRepresentations = There are not other representations of this element.
noStarredItems = You haven't starred any elements yet.
noMembers = This element does not have any members.
noDrafts = You don't have any drafts yet. Only elements that are currently in the draft status will appear here.

cancel = Cancel
next = Next
previous = Previous
finish = Finish
finishAsDraft = Finish as draft
finishAndRelease = Finish and release

validationType = Validationtype
identifier = Identifier
revision = Revision
revisionFormat = Revision {0}
regex = Regex
regexPlaceholder = Enter the regular expression that is used to verify this dataelement
maxLengthPlaceholder = The maximum length
saveAsNewElement = Save as new element
revisions = Revisions
recycle = Recycle
release = Release this element
markToCompare = Compare this group
compareNow = Compare now
markToExport = Export this element
duplicate = Duplicate this element
changeOwner = Change the owner
pleaseWait = Please wait
releaseAllMarked = Release all marked elements

deselectAll = Deselect all subcodes
selectAll = Select all subcodes

changeOwnerWarning = You are currently the owner of this namespace and you are going to change the owner of this namespace. You can not revert this action. Only the owner of this namespace can change its labels, etc. Be careful!

hideNamespace = Hide your namespace

slotName = Name
slotValue = Value

slotNamePlaceholder = Name
slotValuePlaceholder = Value

permittedValue = Permitted value

selectCatalog = Select a catalog
catalogCodes = Catalog codes

withinRange = Within range
range = Range (from - to)
unitOfMeasure = Unit of measure
numRange = Range

datatype = Datatype
withDays = With days
withSeconds = With seconds
useRegex = Use Regex
maxLength = Max length
dateRepresentation = Date representation
timeRepresentation = Time representation

emptyDefinition = Missing field:
emptyDesignation = Missing field:
emptyValue = Missing field:
emptyName = Missing field:
emptyDefinitionDetail = The definition must not be empty!
emptyDesignationDetail = The designation must not be empty!
emptyValueDetail = The value must not be empty!
emptyNameDetail = The name must not be empty!

invalidCatalog = Invalid catalog:
invalidCatalogDetail = The catalog you uploaded is not valid! Please check its format!

invalidCatalogError = Invalid catalog:
invalidCatalogErrorDetail = The catalog you uploaded is not valid: {0}!

invalidCodeCount = Invalid catalog:
invalidCodeCountDetail = The catalog has more or less valid codes than the old one!

invalidCode = Invalid catalog:
invalidCodeDetail = The catalog contains an invalid code: {0}!

invalidInteger = Invalid integer:
invalidIntegerDetail = The range is not a valid integer range!

invalidRange = Invalid range:
invalidRangeDetail = The entered range is not valid!

invalidRegex = Invalid regex:
invalidRegexDetail = The entered regular expression is not valid!

duplicateIdentifier = Duplicate identifier:
duplicateIdentifierDetail = This identifier is already in use, please select another identifier!

invalidIdentifier = Invalid identifier:
invalidIdentifierDetail = The identifier you specified is not valid!

duplicateValue = Duplicate value:
duplicateValueDetail = A permitted value is used at least twice. This is not a valid value domain.

catalogNotSelected = No catalog selected:
catalogNotSelectedDetail = Please select a catalog to continue.

searchPlaceholder = Search for elements that contain this text
search = Search
searchResults = Search results
searchFor = Search for
searchInNamespace = Search in namespace
searchStatus = Search for status

members = Members and subgroups
membersShort = Members
entries = Entries
assignMembers = Assign members and subgroups
assignEntries = Assign record entries

DATAELEMENT = Dataelements
DATAELEMENTGROUP = Dataelementgroups
RECORD = Records
CATALOG = Catalogs
CODE = Codes

DRAFT = Draft
RELEASED = Released
OUTDATED = Outdated

LIST = List of permitted values
INTEGER = Integer
FLOAT = Float
BOOLEAN = Boolean
STRING = String
DATE = Date
DATETIME = Datetime
TIME = Time

DE = Deutsch
EN = English
FR = Francais
ES = Espanol

noDefinitions = No definitions:
noDefinitionsDetail = You need to enter at least one definition and designation

unknownUser = User not found
unknownUserDetail = The user does not exist

elementNotFound = The element does not exist
elementNotFoundDetail = The element you requested does not exist or you do not have the necessary permissions to access this element.

close = Close
delete = Delete element
deleteDialogTitle = Delete element?
deleteDialogSummary = Do you really want to delete this element? It will be recoverable after this operation.

update = Update
updateGroup = Update group
updateDialogTitle = Update group?
updateDialogSummary = Do you really want to update this data element group and all subgroups recursively? This action may take a while, please be patient!
notUpdatedIgnored = Elements that haven't changed are not shown in this list.
executeUpdate = Execute Update

oldElements = Old elements
newElements = New elements

noUpdate = There aren't any elements that can be updated.

errorOccured = An error occurred.
app_inactive = This application has been deactivated by the Samply Auth Administrator.
denied = We are sorry that you don't trust us.
elementNotFound = We are sorry, but the website or element you requested does not exist.
serverError = We are sorry, but a server error occurred
invalidToken = The OAuth2 Provider is not configured properly. Check the public key.

editConstraints = Edit constraints
edit = Edit
editElement = Edit this element
changeLabels = Change labels
asNewElement = Change as new element
useAsTemplate = Use this element as template
misc = Misc.
getOtherRepresentations = Find other representations

invalidMemberCount = Invalid member count:
invalidMemberCountDetail = This record changes the number or elements.

invalidElement = Invalid member:
invalidElementDetail = You have an invalid member in your record: {0}

errors = This request caused an error:

emptyList = We are sorry, but this list is empty

LOCAL_DATE = The local date format
ISO_8601 = YYYY-MM-DD, YYYY-MM (ISO 8601)
DIN_5008 = DD.MM.YYYY, MM.YYYY (DIN 5008, ...)

LOCAL_TIME = The local time format
HOURS_24 = The 24 hours time format (22:34:54, 22:34)
HOURS_12 = The 12 hours time format (11:34:54 PM, 11:34 PM)

READ = read
READ_WRITE = read, write

LOCALMDR = Local MDR
CADSR = caDSR

ELEMENTS = Elements
NAMESPACES = Namespaces

indexTitle = The Metadata Repository (MDR)

introductionOSSE = <p>The OSSE Metadata Repository serves the harmonization of data content. For this purpose it contains a collection of metadata (definitions of data elements), which the registry compounds record in addition to the minimal data set for rare diseases, which is still contained. The more registries define their data elements in a comprehensible way and use identical data definitions, the easier it is to use the data for overarching questions. In defining the data elements, it is important that their meaning is described accurately and that existing data elements are used if possible.</p><p>Please be careful that you do not enter contents that break current law or third-party rights and be aware that the contents are visible and reusable for all registered OSSE users!</p>
introductionSamply = <p>The Samply Metadata Repository serves the harmonization of data content. For this purpose it contains a collection of metadata (definitions of data elements), which the registry compounds record in addition to the minimal data set for rare diseases, which is still contained. The more registries define their data elements in a comprehensible way and use identical data definitions, the easier it is to use the data for overarching questions. In defining the data elements, it is important that their meaning is described accurately and that existing data elements are used if possible.</p><p>Please be careful that you do not enter contents that break current law or third-party rights and be aware that the contents are visible and reusable for all registered users!</p>

importExternalText = <h3>Note</h3>  <p>You are about to import a dataelement form an external MDR. Keep in mind that in this case you:</p><ul><li><b>must not</b> change the <b>meaning</b> of the element</li><li><b>may</b> translate the definitions and designations into your language or add another definition and designation</li><li><b>must</b> apply the value domain as accurately as possible</li></ul>

serverMaintenance = Server maintenance
serverMaintenanceRequired = This server is currently under maintenance. Please come back later.

databaseUpgradeNecessary = This application needs to upgrade the database. This process may take a long time. Please read the documentation before you proceed.
backupRecommended = A database backup is strongly recommended!
startUpgradeDry = Start Upgrade (Dry Run)
startUpgrade = Start upgrade

dryRunSuccessful = The dry run was successful.
contactServerAdmin = There has been an error. Please contact the server administrator to access the logs. Post the error messages on the project website.

languageMissing = Language missing:
languageMissingDetail = The required language "{0}" is missing!
valueLanguageMissing = Language missing:
valueLanguageMissingDetail = The required language "{1}" is missing for the permissible value "{0}"!
slotMissing = Slot missing:
slotMissingDetail = The required slot "{0}" is missing!

pleaseBePatient = Please be patient, the server processes your request...

autoTranslation=Propose a translation for the designation of the first listed definition to the selected language. This feature uses an external webservice to get the translations. They have to be verified by the user.